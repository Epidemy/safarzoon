import React from 'react';
// import './App.css';
import Header from './components/Header'
import Footer from './components/Footer'
import BodyContainer from './components/BodyContainer'

function App() {
  return (
    <div className="App">
      <Header />
      <BodyContainer />
      <Footer />
    </div>
  );
}

export default App;
