const ApiEndpoints = {
    HomePageStatistic: "",
    PlacesInfo: "",
    YourHamsafarZoons: "https://api.myjson.com/bins/1gxaoj",
    Comments: "",
    FAQPage: "",
    ContactUs:"",
    AboutUsContent:"",
    OurPartners: "",
    MostFavoritePlaces:"https://api.myjson.com/bins/wdub7",
    UserInfo: ""
}

export const ReadOnlyEndpoints = {
    HomePageStatistic: "",
    PlacesInfo: "",
    YourHamsafarZoons: "https://api.myjson.com/bins/1gxaoj",
    Comments: "",
    FAQPage: "",
    ContactUs: "",
    AboutUsContent: "",
    OurPartners:"",
    MostFavoritePlaces:"https://api.myjson.com/bins/wdub7", 
    UserInfo: "" 
}

export default ApiEndpoints