import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import ReadOnlyEndpoints from '../Data/ApiEndpoints';



class YourHamsafarZoons extends Component {
    state = {
        People: []
    }

    componentDidMount() {

        fetch(ReadOnlyEndpoints.YourHamsafarZoons)
            .then(response => response.json())
            .then(person => {
                this.setState(prevState => {
                    return {
                        ...prevState,
                        People: person,
                    }
                })
            })
    }

    render() {
        const styles={
            fontSize: "60%"
        }
        return (
            <div>
                <Row className="d-flex flex-row-reverse">
                <p className="text-right m-1">همسفرزونی های شما</p>
                </Row>
                <Row className="d-flex flex-row flex-wrap justify-content-between align-content-around">
                    {this.state.People.map(divItem => {
                        return (
                            <Col className="p-2" key={divItem.id} md={3}>
                                <img className="img-fluid rounded-circle" src={divItem.img} alt={divItem.placeName} width="100%" />
                                <h6 className="text-center text-nowrap font-weight-bold" style={styles}>{divItem.personName}</h6>
                            </Col>
                        )
                    })}
                </Row>
            </div>
        )
    }
}

export default YourHamsafarZoons
