import React from "react";
// import { Col } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

const ChooseDay = () => {

    return (
        <div className="d-flex flex-row-reverse justify-content-center">
            <button type="button" className="btn m-1" style={{backgroundColor: '#f1f1f1', color:'black'}}>
                1
            </button>
            <button type="button" className="btn btn-dark m-1 active">
                روز 2
            </button>
            <button type="button" className="btn m-1" style={{backgroundColor: '#f1f1f1', color:'black'}}>
                3
            </button>
            <button type="button" className="btn m-1" style={{backgroundColor: '#f1f1f1', color:'black'}}>
                4
            </button>
            <button type="button" className="btn m-1" style={{backgroundColor: '#f1f1f1', color:'black'}}>
                5
            </button>
        </div>
    )
}
export default ChooseDay