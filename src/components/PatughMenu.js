import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import './PatughMenu.Style.css';



class PatughMenu extends Component {
    state = {
        ishover: "false"
    }

    handleClick = () => {
        this.setState({
            ishover: "true"
        })
    }

    render() {
        return (            
            <Container>
                <Row className="d-flex flex-row-reverse justify-content-between mt-1">
                    <Col md={3} className="button bg-warning text-center pt-1 pb-1">برنامه سفر</Col>
                    <Col md={3} className="button bg-white text-center pt-1 pb-1">محل اقامت</Col>
                    <Col md={3} className="button bg-white text-center pt-1 pb-1">مرور سریع</Col>
                    <Col md={3} className="button bg-white text-center pt-1 pb-1">با چی برم</Col>
                </Row>
            </Container>
        )
    }
}

export default PatughMenu
