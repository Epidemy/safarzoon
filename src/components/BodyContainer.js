import React from "react";
import { Container, Row, Col } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

import safarzoon from '../img/safarzoon.png';

import UserInfo from '../components/UserInfo'
import RightSliderMenu from '../components/RightSliderMenu'
import PatughMenu from '../components/PatughMenu'
import ChooseDay from '../components/ChooseDay'
import MostFavPlaces from '../components/MostFavPlaces'
import YourHamsafarZoons from '../components/YourHamsafarZoons'


const BodyContainer = () => {
    return (
        <Container className="bg-light clearfix">
            <Row className="d-flex flex-row-reverse justify-content-md-around mt-1">
                <Col md={3} className="bg-light mt-4 mr-3">
                    <UserInfo />
                    <RightSliderMenu/>
                    <MostFavPlaces />
                    <Row className="">
                    <img src={safarzoon} alt="safarzoon" width="100%"/>
                    </Row>
                    <YourHamsafarZoons />
                    <Row className="mb-3">
                    <img src={safarzoon} alt="safarzoon" width="100%"/>
                    </Row>
                </Col>
                <Col md={8} className="bg-light mt-4 ml-2">
                    <PatughMenu />
                    <ChooseDay />
                    <hr
                        style={{
                            color: "yellow",
                            backgroundColor: "yellow",
                            size: "20"
                        }}
                    />
                </Col>
            </Row>
        </Container>

    )
}
export default BodyContainer