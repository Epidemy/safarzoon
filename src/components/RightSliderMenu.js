import React from "react";
import { Row } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import EarthW from '../img/EarthW.svg';


const RightSliderMenu = () => {
    return (
        <div>            
            <button type="button" className="btn btn-dark flex-fill btn-block m-1 p-0">
                <Row className="d-flex flex-row-reverse justify-content-md-around mr-1">
                    <img src={EarthW} alt="Earth" width="7%" margin="right-3" />
                    <p className="ml-4"> دخیره برنامه </p>
                </Row>
            </button>
            <button type="button" className="btn btn-dark flex-fill btn-block m-1 p-0">
                <Row className="d-flex flex-row-reverse justify-content-md-around mr-2">
                    <img src={EarthW} alt="Earth" width="7%" margin="right-3" />
                    <p className="ml-4"> انتشار در پاتوق </p>
                </Row>
            </button>
            <button type="button" className="btn btn-dark flex-fill btn-block m-1 p-0">
                <Row className="d-flex flex-row-reverse justify-content-md-around mr-3">
                    <img src={EarthW} alt="Earth" width="7%" margin="right-3" />
                    <p className="ml-4 mr-0"> خروجی برنامه سفر </p>
                </Row>
            </button>
            <button type="button" className="btn btn-primary flex-fill btn-block m-1 p-0 active">
                <Row className="d-flex flex-row-reverse justify-content-md-around mr-2">
                    <img src={EarthW} alt="Earth" width="7%" margin="right-3" />
                    <p className="ml-4 mr-0"> مسیر روزانه </p>
                </Row>
            </button>
        </div>
    )
}
export default RightSliderMenu