<ContaiNavner className="vw-100">
                <Row className="d-flex flex-row-reverse mt-2 mb-2 shadow">
                    <Col md={7} className="justify-content-md-center">
                        <Row className="d-flex flex-row-reverse">
                            <Col md={1} className="justify-content-md-center">
                                <img src={Menu} alt="Menu" width="70%" />
                            </Col>
                            <Col className="justify-content-md-center">
                                <img src={Logo} alt="Logo" width="100%" />                                
                            </Col>
                            <Col className="justify-content-md-around">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={SpeechBubble} alt="SpeechBubble" width="20%" />
                                    <p> پاتوق </p>
                                </Row>
                            </Col>
                            <Col className="justify-content-md-center">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={Earth} alt="Earth" width="20%" />
                                    <p> سفرگردی </p>
                                </Row>
                            </Col>
                            <Col className="justify-content-md-center">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={AirplaneSymbol} alt="AirplaneSymbol" width="20%" />
                                    <p> خدمات گردشگری </p>
                                </Row>
                            </Col>
                            <Col className="justify-content-md-center">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={Magazine} alt="Magazine" width="20%" />
                                    <p> مجله گردشگری </p>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5}>
                        <Col md={4} className="m-2">
                            <Button color="warning" size="sm">عضویت <span textColor="White">|</span> ورود</Button>
                        </Col>
                    </Col>
                </Row>
            </Container>

            // <nav className="navbar navbar-expand-lg navbar-light bg-light d-flex flex-row-reverse">
            //     <img src={Menu} alt="Menu" width="2%" />
            //     <img src={Logo} alt="Logo" width="5%" />

            //     <div className="collapse navbar-collapse" id="navbarSupportedContent">
            //         <ul className="navbar-nav mr-auto flex-row-reverse">
            //             <li class="nav-item">
            //                 <img className="icon" src={SpeechBubble} alt="SpeechBubble" width="10%" />
            //                 <p className="navItem p-1"> پاتوق </p>
            //             </li>
            //             <li class="nav-item">
            //                 <img src={this.state.earthIcon} alt="Earth" width="10%" margin="left-1" />
            //                 <p className="navItem p-1"> سفرگردی </p>
            //             </li>
            //             <li class="nav-item">
            //                 <img src={AirplaneSymbol} alt="AirplaneSymbol" width="10%" onMouseOver={this.state.handleMouseOver} onMouseOut={this.state.handleMouseOut} />
            //                 <p className="navItem"> خدمات گردشگری </p>
            //             </li>
            //             <li class="nav-item">
            //                 <img src={Magazine} alt="Magazine" width="10%" margin="left-1" />
            //                 <p className="navItem"> مجله گردشگری </p>
            //             </li>
            //         </ul>
            //     </div>

            //     <Button rounded="pill-sm" color="warning" size="sm">عضویت <span color="text-white">|</span> ورود</Button>
            // </nav>
            //             <nav class="navbar navbar-expand-lg navbar-light bg-light">
            //   <a class="navbar-brand" href="#">Navbar</a>
            //   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            //     <span class="navbar-toggler-icon"></span>
            //   </button>

            //   <div class="collapse navbar-collapse" id="navbarSupportedContent">
            //     <ul class="navbar-nav mr-auto">
            //       <li class="nav-item active">
            //         <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            //       </li>
            //       <li class="nav-item">
            //         <a class="nav-link" href="#">Link</a>
            //       </li>
            //       <li class="nav-item">
            //         <a class="nav-link disabled" href="#">Disabled</a>
            //       </li>
            //     </ul>
            //     <form class="form-inline my-2 my-lg-0">
            //       <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            //       <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            //     </form>
            //   </div>
            // </nav>