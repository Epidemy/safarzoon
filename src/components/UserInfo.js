import React, { Component } from "react";
import { Container, Row, Col, Progress } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

import ReadOnlyEndpoints from '../Data/ApiEndpoints';
import MyPhoto from '../img/MyPhoto.png';
import SpeechBubble from '../img/SpeechBubble.svg';

class UserInfo extends Component {
    state = {
        userPhoto: MyPhoto,
        userRequest: "برنامه سفر 4 روزه اصفهان",
        userInfo: "فاطمه ریاحی",
        userScore: null
    }

    componentDidMount() {

        fetch(ReadOnlyEndpoints.UserInfo)
            .then(response => response.json())
            .then(Person => {
                this.setState(prevState => {
                    return {
                        ...prevState,
                        People: Person,
                    }
                })
            })
    }

    render() {
        return (
            <Container>
                <Row className="d-flex flex-row-reverse">
                    <Col md={3}>
                        <img src={this.state.userPhoto} className="rounded-circle" alt="user" width="100%" />
                    </Col>
                    <Col md={9}>
                        <p className="text-right mb-1">{this.state.userRequest}</p>
                        <h6 className="text-right"> {this.state.userInfo}</h6>
                    </Col>
                </Row>

                <Row className="progressBar d-flex flex-row-reverse justify-content-between">
                    <Col md={1}>
                        <img className="icon" src={SpeechBubble} alt="SpeechBubble" width="100%" />
                    </Col>
                    <Col md={10}>
                        <Progress animated color="success" value="3000" max={3000}>3000</Progress>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default UserInfo