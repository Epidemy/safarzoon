import React, { Component } from 'react'
import { Container, Row, Col, Button } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import './Header.Style.css';

import Logo from '../img/Logo.png';
import Menu from '../img/Menu.svg';
import SpeechBubble from '../img/SpeechBubble.svg';
import EarthB from '../img/EarthB.svg';
import EarthW from '../img/EarthW.svg';
import AirplaneSymbol from '../img/AirplaneSymbol.svg';
import Magazine from '../img/Magazine.svg';


class Header extends Component {
    state = {
        earthIcon: EarthB,
        Styles: {
            color: "Black",
        }
    }

    handleClick = (e) => {
        const CleckStyle = {
            color: "yellow",
        }

        this.setState(prevState => {
            return {
                earthIcon: EarthW,
                ...prevState,
                Styles: CleckStyle,
            }
        })
    }


    render() {
        return (
            <Container className="vw-100 p-0">
                <Row className="d-flex flex-row-reverse align-content-around shadow p-0">
                    <Col md={7} className="justify-content-md-around p-0 m-0">
                        <Row className="d-flex flex-row-reverse">
                            <Col md={1} className="justify-content-md-center pt-2 pb-2 m-0">
                                <img src={Menu} alt="Menu" width="75%" />
                            </Col>
                            <Col className="justify-content-md-center pb-2 pt-2 m-0">
                                <img src={Logo} alt="Logo" width="100%" />
                            </Col>
                            <Col className="navItem justify-content-md-around">
                                <Row className=" d-flex flex-row-reverse" onClick={this.handleClick}>
                                    <img className="icon" src={SpeechBubble} alt="SpeechBubble" width="20%" />
                                    <p className="pb-2 pt-2 pr-1 m-0"> پاتوق </p>
                                </Row>
                            </Col>
                            <Col className="navItem justify-content-md-center">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={this.state.earthIcon} alt="Earth" width="20%" />
                                    <p className="pb-2 pt-2 pr-1 m-0"> سفرگردی </p>
                                </Row>
                            </Col>
                            <Col className="navItem justify-content-md-center">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={AirplaneSymbol} alt="AirplaneSymbol" width="20%" />
                                    <p className="pb-2 pt-2 m-0"> خدمات گردشگری </p>
                                </Row>
                            </Col>
                            <Col className="navItem justify-content-md-center">
                                <Row className="d-flex flex-row-reverse">
                                    <img src={Magazine} alt="Magazine" width="20%" />
                                    <p className="pb-2 pt-2 m-0"> مجله گردشگری </p>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={5}>
                        <Col md={4} className="p-2">
                            <Button rounded="pill-sm" color="warning" size="sm">عضویت <span color="text-white">|</span> ورود</Button>
                        </Col>
                    </Col>
                </Row>
            </Container>
        )
    }
}
export default Header