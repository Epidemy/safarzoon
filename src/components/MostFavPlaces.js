import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import ReadOnlyEndpoints from '../Data/ApiEndpoints';



class MostFavPlaces extends Component {
    state = {
        Places: []
    }

    componentDidMount() {

        fetch(ReadOnlyEndpoints.MostFavoritePlaces)
            .then(response => response.json())
            .then(Place => {
                this.setState(prevState => {
                    return {
                        ...prevState,
                        Places: Place,
                    }
                })
            })
    }

    render() {
        return (
            <div>
                <Row className="d-flex flex-row-reverse">
                <p className="text-right m-1">پرطرفدارترین جاذبه های این هفته</p>
                </Row>
                <Row className="d-flex flex-row flex-wrap justify-content-between align-content-around">
                    {this.state.Places.map(divItem => {
                        return (
                            <Col className="p-2" key={divItem.id} md={3}>
                                <img className="img-fluid rounded" src={divItem.img} alt={divItem.placeName} width="100%" />
                            </Col>
                        )
                    })}
                </Row>
            </div>
        )
    }
}

export default MostFavPlaces
