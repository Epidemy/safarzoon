import React from 'react'
import { Container, Row, Col } from "reactstrap";
// import { Link } from "rct-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

const dataFooter = [
    {
        id: "1",
        title: "راهنما",
        listItems: [
            {
                id: "1",
                text: "سوالات متداول",
                link: "#"
            },
            {
                id: "2",
                text: "ارتباط با ما",
                link: "#"
            },
        ]
    },
    {
        id: "2",
        title: "درباره ما",
        listItems: [
            {
                id: "3",
                text: "درباره ما",
                link: "#"
            },
            {
                id: "4",
                text: "همکاران ما",
                link: "#"
            },
        ]
    },
    {
        id: "3",
        title: "سایر",
        listItems: [
            {
                id: "5",
                text: "برنامه ریزی ها",
                link: "#"
            },
            {
                id: "6",
                text: "مقصدها",
                link: "#"
            },
            {
                id: "7",
                text: "آعتبارات",
                link: "#"
            }
        ]
    }
]

const Footer = () => {
    return (
        <footer>
            <Container className="bg-light clearfix">
                <Row className="d-flex flex-row-reverse justify-content-md-center mt-2">
                    {dataFooter.map(divItem => {
                        return <Col key={divItem.id} md={2} className="d-flex flex-column text-right font-weight-light">
                            <h6 className="font-weight-bold text-warning">
                                {divItem.title}
                            </h6>
                            <ul className="list-unstyled">
                                {divItem.listItems.map(item => {
                                    return <li key={item.id}>
                                        {/* <a href={item.link}> */}
                                        {item.text}
                                        {/* </a> */}
                                    </li>
                                })}
                            </ul>
                        </Col>
                    })}
                </Row>
            </Container>

            <Container className="bg-warning">
                <Row className="justify-content-md-center pt-2">
                    <p className="align-middle">&copy; تمام حقوق و مالکیت سایت متعلق به سفرزون است | 2018 </p>
                </Row>
            </Container>
        </footer>
    );
};

export default Footer;